import 'package:flutter/material.dart';
import 'package:university/Screens/university_list_screen.dart';

class UniversityDetailScreen extends StatelessWidget {
  static const String routeName = '/details';

  static Route route() {
    return MaterialPageRoute(
        settings: const RouteSettings(name: routeName),
        builder: (_) => const UniversityListScreen());
  }

  const UniversityDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Details Screen'),
      ),
      body: Container(),
    );
  }
}
