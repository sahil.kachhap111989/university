import 'package:flutter/material.dart';

class UniversityListScreen extends StatefulWidget {
  static const String routeName = '/list';

  static Route route() {
    return MaterialPageRoute(
        settings: const RouteSettings(name: routeName),
        builder: (_) => const UniversityListScreen());
  }

  const UniversityListScreen({Key? key}) : super(key: key);

  @override
  State<UniversityListScreen> createState() => _UniversityListScreenState();
}

class _UniversityListScreenState extends State<UniversityListScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('List Screen'),
      ),
      body: Container(),
    );
  }
}
