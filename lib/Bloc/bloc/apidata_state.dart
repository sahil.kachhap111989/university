part of 'apidata_bloc.dart';

@immutable
abstract class ApidataState extends Equatable{}

class ApidataInitial extends ApidataState {
  @override
  
  List<Object?> get props => [];
}

class DataLoadedState extends ApidataState{
  final List<University> university;

  DataLoadedState(this.university);

  @override

  List<Object?> get props => [university];
}

class DataErrorState extends ApidataState{
  final String error;

  DataErrorState(this.error);

  @override
  
  List<Object?> get props => [error];
}


