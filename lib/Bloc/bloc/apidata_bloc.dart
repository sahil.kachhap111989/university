import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:university/Data/university_model.dart';
import 'package:university/Data/university_repository.dart';
part 'apidata_event.dart';
part 'apidata_state.dart';

class ApidataBloc extends Bloc<ApidataEvent, ApidataState> {
  final UniversityRepository _universityRepository;
  ApidataBloc(this._universityRepository) : super(ApidataInitial()) {
   
    on<ApidataEvent>((event, emit) async{
      try{
       final university = await _universityRepository.getUniversityInfo();
       emit(DataLoadedState(university!));
      }catch(error){
        emit(DataErrorState(error.toString()));
      }
    });
  }
}
