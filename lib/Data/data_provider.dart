import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;

class DataProvider{
  static final BaseUrl = "http://universities.hipolabs.com/search?country=United+States";


  static Future<http.Response?> fetchData() async{
    try{
    final parsedUrl = Uri.parse(BaseUrl);
    final http.Response apiResponse = await http.get(parsedUrl);
    print(apiResponse);
    return apiResponse;
    }catch(e){
      log(e.toString());
    }
  }
}