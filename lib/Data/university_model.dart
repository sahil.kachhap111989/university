import 'dart:convert';

List<University>? ModelFromJson(String json) => List<University>.from(
    jsonDecode(json).map((data) => University.fromJson(data)));

String ModelToJson(List<University> modelData) =>
    jsonEncode(List<dynamic>.from(modelData.map((data) => data.toJson())));

class University {
  String? country;
  String? name;
  List<String>? domains;
  String? alphaTwoCode;
  List<String>? webPages;

  University(
      {this.country,
      this.name,
      this.domains,
      this.alphaTwoCode,
      this.webPages});

  factory University.fromJson(Map<String, dynamic> json) => University(
      country: json['country'],
      name: json['name'],
      domains: json['domains'].cast<String>(),
      alphaTwoCode: json['alpha_two_code'],
      webPages: json['web_pages'].cast<String>());

  Map<String, dynamic> toJson() => {
        'country': country,
        'name': name,
        'domains': domains,
        'alpha_two_code': alphaTwoCode,
        'web_pages': webPages,
      };
}
