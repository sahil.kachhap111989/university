import 'dart:convert';
import 'dart:developer';

import 'package:university/Data/data_provider.dart';
import 'package:university/Data/university_model.dart';
import 'package:http/http.dart' as http;

class UniversityRepository{
  Future<List<University>?> getUniversityInfo() async{
    try{
    final http.Response? response = await DataProvider.fetchData();
       List<University>? _universities = ModelFromJson(response!.body);
       return _universities;
    }catch(error){
      log(error.toString());
    }
  }
}