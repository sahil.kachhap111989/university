import 'package:flutter/material.dart';
import 'package:university/Screens/university_details_screen.dart';
import 'package:university/Screens/university_list_screen.dart';

class AppRouter {
  static Route onGenerateRoute(RouteSettings settings) {
    print('${settings.name}');

    switch (settings.name) {
      case '/':
        return UniversityListScreen.route();
      case UniversityListScreen.routeName:
        return UniversityListScreen.route();
      case UniversityDetailScreen.routeName:
        return UniversityDetailScreen.route();
      default:
        return _errorRoute();
    }
  }

  static Route _errorRoute() {
    return MaterialPageRoute(
        builder: (_) => Scaffold(
              appBar: AppBar(
                title: const Text('Error'),
              ),
            ),
        settings: const RouteSettings(name: '/error'));
  }
}
